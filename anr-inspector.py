#!/usr/bin/python

import sys
import re
import os

class AndroidEnvironment:
    def __init__(self, text):
        print("Creating Environment...")

        # A dictionary of AndroidProcess objects {pid : AndroidProcess}
        self.processes = []

        # Parse the text and create the processes
        self._read_processes(text)

    def _read_processes(self, text):
        newLine = '[\n\r]*'

        #             ----- pid 908 at 2012-07-12 12:01:39 -----
        pidStart = r'^----- pid ([0-9]+) .*? -----'

        #           Cmd line: system_server
        cmdLine  = 'Cmd line: ([^\n\r]*)'

        lines = '(.+?)'

        #          ----- end 908 -----
        pidEnd = r'----- end \1 -----'

        regex = re.compile(
            pidStart + newLine +
            cmdLine  + newLine +
            lines    +
            pidEnd
        , re.MULTILINE | re.DOTALL);

        matches = regex.findall(text)

        if (len(matches) < 1):
            raise Exception("Found no processes.")

        print("Found {0} processes".format(str(len(matches))))
        n = 0

        for process in matches:
            n += 1
            pid   = process[0]
            pname = process[1]
            ptext = process[2]

            self.processes.append(AndroidProcess(pname, pid, n, self, ptext))

class AndroidProcess:
    def __init__(self, commandLine, pid, number, environment, text):
        print("Creating Process {0} ({1})".format(pid, commandLine))

        self.pid         = pid
        self.number      = number
        self.commandLine = commandLine

        # The parent environment
        self.environment = environment

        # A dictionary of AndroidThread objects {tid : AndroidThread}
        self.threads = {}

        # A dictionary of AndroidLock objects {objectId : AndroidLock}
        self.locks = {}

        self._read_threads(text)
        self._read_locks(text)

    def _read_threads(self, text):
        newLine = '[\n\r]*'

        #           "AsyncTask #4" prio=5 tid=26 Blocked
        thread = r'^"([^"]*)".* tid=([0-9]+) (.*)$'

        #                 | group="main" sCount=1 dsCount=0 obj=0x417f0628 self=0x1001ba0
        pipeObject = r'  \| .* obj=(0x[0-9a-f]+).*'

        regex = re.compile(
                thread + newLine +
                pipeObject
        , re.MULTILINE)

        # Every match looks like this:
        #   Thread Name     Thread ID Thread OP  Thread Object Ref
        # ('AsyncTask #4', '26',      'Blocked',    '0x417f0628'
        matches = regex.findall(text)

        print("Found {0} threads".format(str(len(matches))))

        for thread in matches:
            name     = thread[0]
            tid      = thread[1]
            state    = thread[2]
            objectId = thread[3]

            if tid in self.threads:
                raise Exception("A process (" + self.pid + ") already has a thread with this id (" + tid + ")")

            self.threads[tid] = AndroidThread(tid, name, objectId, state, self)

    def _read_locks(self, text):
        newLine = '[\n\r]*'

        #           "AsyncTask #4" prio=5 tid=26 Blocked
        thread = r'^"([^"]*)".* tid=([0-9]+) (.*)$'

        #                    | group="main" sCount=1 dsCount=0 obj=0x417f0628 self=0x1001ba0
        pipes = '([\n\r]*  \\| .*)*'

        #        at java.lang.Object.wait(Native Method)
        at = r'  at (.*)'

        #             - waiting to lock <0x414aca88> (a java.lang.StringBuilder) held by thread 20 (Binder Thread #3)
        waiting = r'  - waiting to lock <(0x[0-9a-f]+)> \(a ([^\)]*)\) held by (thread |tid=)([0-9]+)'

        regex = re.compile(
                thread     + newLine +
                pipes      + newLine +
                at         + newLine +
                waiting
        , re.MULTILINE)

        # Every match looks like this:
        #   Thread Name [0]  Thread ID [1]  Thread OP [2]  [3]    Code Reference [4]                                                                                             Lock Object [5]  Lock Object Type [6]                            Holding Thread ID [7]
        # ('main',           '1',           'MONITOR',     '...', 'com.android.server.am.ActivityManagerService$AppDeathRecipient.binderDied(ActivityManagerService.java:~862)', '0x414ab128',    'com.android.server.am.ActivityManagerService', '13'                )
        matches = regex.findall(text)

        print("Found {0} locks".format(str(len(matches))))

        for lock in matches:
            blockedThreadId = lock[1]
            codeReference   = lock[4]
            objectId        = lock[5]
            objectType      = lock[6]
            # We don't need No. 7
            holdingThreadId = lock[8]

            self._create_lock(objectId, objectType, blockedThreadId, codeReference, holdingThreadId)

    def _create_lock(self, objectId, objectType, blockedThreadId, codeReference, holdingThreadId):
        if blockedThreadId not in self.threads:
            raise Exception("Trying to add lock (" + objectId + ") in a process (" + self.pid + "), but there's no thread with this id: " + blockedThreadId + " (blocked)")

        if holdingThreadId not in self.threads:
            raise Exception("Trying to add lock (" + objectId + ") in a process (" + self.pid + "), but there's no thread with this id: " + holdingThreadId + " (holding)")

        blockedThread = self.threads[blockedThreadId]
        holdingThread = self.threads[holdingThreadId]

        lock = None
        if objectId in self.locks:
            lock = self.locks[objectId]
        else:
            lock = AndroidLock(objectId, objectType, holdingThread, self)
            self.locks[objectId] = lock

        if not lock.holdingThread == holdingThread:
            raise Exception("Lock #" + objectId + " (" + objectType + ") is held by Thread #" + lock.holdingThread.tid + " (" + lock.holdingThread.name
                    + "), but it's also held by thread #" + holdingThread.tid + " (" + holdingThread.name + ")")

        lock.blockedThreads.append((blockedThread, codeReference))

class JavaObject:
    def __init__(self, objectId):
        self.objectId = objectId

    def get_objectId(self):
        return 'obj' + self.objectId

class AndroidThread(JavaObject):
    def __init__(self, tid, name, objectId, state, process):
        JavaObject.__init__(self, objectId)

        print("Creating thread #{0} ({1}) for process #{2}".format(tid, name, process.pid))

        self.tid      = tid
        self.name     = name
        self.objectId = objectId
        self.state    = state

        # Its parent process
        self.process  = process

class AndroidLock(JavaObject):
    def __init__(self, objectId, objectType, holdingThread, process):
        JavaObject.__init__(self, objectId)

        print("Creating lock #{0} ({1}) for process #{2}".format(objectId, objectType, process.pid))

        self.objectId       = objectId
        self.objectType     = objectType
        self.process        = process

        self.holdingThread  = holdingThread
        self.blockedThreads = []

def read_file(filename):
    # Open the File
    f = open (filename, "r")

    try:
        #Read the entire file
        data = f.read()
    finally:
        # Close the file
        f.close()

    return data

def write_file(filename, text):
    # Open the File
    f = open (filename, "w")

    try:
        #Write the lines
        f.write(text)
    finally:
        # Close the file
        f.close()

def create_dot(environment, directory):
    for process in environment.processes:
        if (process.locks):
            create_dot_for_process(process, directory)

#def get_objectId(obj):
#    return 'obj' + obj.objectId

def create_dot_for_process(process, directory):
    filename = directory + '/' + str(process.number) + '-pid-' + str(process.pid) + '-' + process.commandLine
    filenameDot = filename + '.dot'
    filenamePng = filename + '.png'

    # Open the File
    f = open(filenameDot, "w")

    try:
        f.write('digraph {\n')

        # Write the locks
        f.write('# The vertices (Locks and Threads)\n')

        for lock in process.locks.itervalues():
            lockId   = lock.get_objectId()
            lockType = "" if lock.objectType is None else "\\n" + lock.objectType

            # Write the lock
            f.write('\t' + lockId   + ' [label="#' + lock.objectId + lockType + ' "]\n')

            for blockedThreadInfo in lock.blockedThreads:
                #Write all blocked threads
                thread = blockedThreadInfo[0]
                threadId = thread.get_objectId()

                f.write('\t' + threadId + ' [label="#' + thread.tid + '\\n' + thread.name + '\\n' + thread.state + ' "]\n')

            if (lock.holdingThread is not None):
                thread = lock.holdingThread
                threadId = thread.get_objectId()
                f.write('\t' + threadId + ' [label="#' + thread.tid + '\\n' + thread.name + '\\n' + thread.state + ' "]\n')

            f.write('\n')


        # Write the edges
        f.write('# The edges\n')

        for lock in process.locks.itervalues():
            lockId   = lock.get_objectId()

            for blockedThreadInfo in lock.blockedThreads:
                threadId = blockedThreadInfo[0].get_objectId()
                f.write('\t' + lockId + ' -> ' + threadId + ' [color=red label="blocking\\n' + blockedThreadInfo[1] + '"]\n')

            if (lock.holdingThread is not None):
                threadId = lock.holdingThread.get_objectId()
                f.write('\t' + threadId + ' -> ' + lockId + ' [color=black label=holding]\n')

            f.write('\n')

        f.write('}\n')

    finally:
        # Close the file
        f.close()

    print("Creating PGN from DOT...")
    os.system("dot -Kcirco -Tpng " + filenameDot + " -o " + filenamePng)

if __name__=="__main__":

    try:
        if len(sys.argv) < 2:
            raise Exception("Input file not specified.")

        filename = sys.argv[1]

        print("Opening {0}...".format(filename))
        text = read_file(filename)

        print("Processing...")
        environment = AndroidEnvironment(text)

        outputDirectory = 'out_' + filename
        print("Creating output directory {0}".format(outputDirectory))
        if not os.path.exists(outputDirectory):
            os.makedirs(outputDirectory)

        print("Creating Dot Output...")
        data = create_dot(environment, outputDirectory)

        print("Done.")

    except Exception as e:
        print(e)

